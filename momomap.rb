#!/usr/bin/env ruby

require "json"

class Map
  Point = Struct.new(:x, :y)

  HEIGHT = 34
  WIDTH = 76

  attr_reader :coordinates

  def initialize(coordinates)
    @content = Array.new(HEIGHT) { Array.new(WIDTH) }
    @coordinates = coordinates.map do |c|
      p = Point.new(c["x"].to_i - 1, c["y"].to_i - 1)
      @content[p.y][p.x] = p
    end
  end

  def to_s
    tmp = "┌" + ("─" * WIDTH) + "┐\n"
    @content.each do |row|
      tmp += "│"
      row.each { |x| tmp += x ? "░" : " " }
      tmp += "│\n"
    end
    tmp += "└" + ("─" * WIDTH) + "┘\n"
  end
end

begin
  savefile = ARGV.first
  abort "Requires a valid savefile" unless savefile
  data = File.read(savefile)
  json = JSON.parse(data)
  map = Map.new(json["mapdata"])
  puts map
rescue Errno::EISDIR, Errno::ENOENT
  puts "#{savefile} is not a valid savefile"
  exit 1
end
